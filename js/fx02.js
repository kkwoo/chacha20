let lrot32broke =  (op,amt) => {
          let t01 = op.toString(2).padStart(32, '0')
          let p01 = t01.slice(amt, t01.length)
          let p02 = t01.slice(0, amt)
          // return (p01 + p02)
          return parseInt(p01 + p02, 2)
};

let lrot32 = (op,amt) => {
  return ((op << amt>>>0) | (op >>> (32-amt)))>>>0;
};
let th01 = (v01, v02) => {
	let r01=lrot32(v01,v02);
	console.log("before: " + v01.toString(16));
	console.log("after: " + r01.toString(16));
	console.log(v01.toString(2));
	console.log(r01.toString(2));
};

th01(0xfce2e394, 2);
th01(0x7998bfda, 7);

let qrop = (x, y, z, r) => {
  let x2 = (x + z) % (2**32);
  return [x2,lrot32((y^x2),r)]};

let th02 = (v01, v02, v03, v04) => {
	let r01=qrop(v01,v02,v03,v04);
	console.log("before: " + v01.toString(16) + "," + v02.toString(16) + "," + v03.toString(16) + "," + v04.toString(16));
	console.log("after: " + r01[0].toString(16) + "," + r01[1].toString(16));
	console.log(v01.toString(2));
// 	console.log(r01.toString(2));
};

// from RFC 8439, page 6
//b = 0x01020304
//c = 0x77777777
//d = 0x01234567
th02(0x77777777, 0x01020304, 0x01234567, 7);

let qr_base = ({a , b = 0, c = 0, d = 0}) => {
	return ({a: a, b: b, c: c, d: d});
};

let qr = ({a, b, c, d}) => {
	let qrop1 = qrop(a, d, b, 16);
	let qrop2 = qrop(c, b, qrop1[1], 12);
	let qrop3 = qrop(qrop1[0], qrop1[1], qrop2[1], 8);
	let qrop4 = qrop(qrop2[0], qrop2[1], qrop3[1], 7);
	return ({a: qrop3[0], b: qrop4[1], c: qrop4[0], d: qrop3[1]});
};

let qr2 = ({a, b, c, d}) => {
  a = (a + b) % (2**32); d = lrot32(d ^ a, 16);
  c = (c + d) % (2**32); b = lrot32(b ^ c, 12);
  a = (a + b) % (2**32); d = lrot32(d ^ a, 8);
  c = (c + d) % (2**32); b = lrot32(b ^ c, 7);
  return ({a: a, b: b, c: c, d: d});
};

let qrt01 = qr({a: 0x11111111,b: 0x01020304, c: 0x9b8d6f43, d: 0x01234567});
console.log(qrt01.a.toString(16));
console.log(qrt01.b.toString(16));
console.log(qrt01.c.toString(16));
console.log(qrt01.d.toString(16));
console.log(JSON.stringify(qr({a: 0x11111111,b: 0x01020304, c: 0x9b8d6f43, d: 0x01234567})));
let qrt02 = qr2({a: 0x11111111,b: 0x01020304, c: 0x9b8d6f43, d: 0x01234567});
console.log(qrt02.a.toString(16));
console.log(qrt02.b.toString(16));
console.log(qrt02.c.toString(16));
console.log(qrt02.d.toString(16));
console.log(JSON.stringify(qr2({a: 0x11111111,b: 0x01020304, c: 0x9b8d6f43, d: 0x01234567})));
let qrt03 = qr2({a: 0x516461b1,b: 0x2a5f714c, c: 0x53372767, d: 0x3d631689});
console.log(qrt03.a.toString(16));
console.log(qrt03.b.toString(16));
console.log(qrt03.c.toString(16));
console.log(qrt03.d.toString(16));
console.log(JSON.stringify(qr2({a: 0x11111111,b: 0x01020304, c: 0x9b8d6f43, d: 0x01234567})));
