// fx01.js - practice code


console.log("hello world")
var h01 =0xfce2
console.log(h01.toString(2))
var b01 = h01.toString(2)
var b02a = b01.slice(5,b01.length)
var b02b = b01.slice(0, 5)
console.log(parseInt(b02a,2).toString(16))
console.log(b02b)
console.log(b02a)
console.log(b02a + b02b)
var b03 = parseInt(b02a + b02b,2)
var b03b = b03.toString(16)
console.log(b03b)
console.log("end")

const f01 = (op,amt) => {
	  let t01 = op.toString(2)
	  let p01 = t01.slice(amt, t01.length)
	  let p02 = t01.slice(0, amt)
	  return (p01 + p02)
	  // return parseInt(p01 + p02, 2)
}
// use slice to implement bitwise rotate
//
// next step: modularise string to number encoding
const str2numarr = (p01) => {
  return [...p01].map(
    (c) => {
	      let rawbin = c.codePointAt(0).toString(2);
	      let zeropadding = "0".repeat(32);
	      return zeropadding.substring(0,zeropadding.length - rawbin.length) + rawbin;
    }
  );
};

// str2numarr inverse
const numarr2str = (p01) => {
  return p01.map(
  (c) => {return String.fromCodePoint(parseInt(c,2));}
  ).reduce((acc, cv) => acc + cv);
};

str2numarr("abc");
na: 0, b: 0, c: 0, d: 0umarr2str(str2numarr("abc"));

str2numarr("asdfasdf").map((c) => {return String.fromCodePoint(parseInt(c,2))}).reduce((acc, cv) => acc + cv)


// next step: quarter round steps 1 - 4
const qrs01 = ({a = 0, b = 0, c = 0, d = 0}) => {return {a: a+b, b: b, c: c, d: (d ^ (a+b)) <<< 16} ;}
const qrs02 = ({a = 0, b = 0, c = 0, d = 0}) => {return qrs01({a: a, b: (b ^ (c+d)) <<< 12, c: c + d, d: d}) ;}
const qrs03 = ({a = 0, b = 0, c = 0, d = 0}) => {return qrs02({a: a+b, b: b, c: c, d: (d ^ (a+b)) <<< 8}) ;}
const qrs04 = ({a = 0, b = 0, c = 0, d = 0}) => {return qrs03({a: a, b: (b ^ c) <<< 2, c: c + d, d: d}) ;}
